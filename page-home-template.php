<?php
/**
 * Template Name: Home Page Tpl
 *
 * This is the template that displays the home page by default.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package west-lafayette-library
 */

get_header();
?>
	<div id="home" class="content-area">
		<main id="main" class="site-main">
			<div class="actions-wrapper">
				<div class="gather">
					<h2>Gather</h2>
					<p>We have space for your meetings and events</p>
					<div class="gather-menu">
						<?php
							if(is_active_sidebar('gather-sidebar')){
							dynamic_sidebar('gather-sidebar');
							}
						?>
					</div>
				</div>
				<div class="discover">
					<h2>Discover</h2>
					<p>We'll get you what you're looking for even if we don't have it</p>
					<div class="discover-menu">
						<?php
							if(is_active_sidebar('discover-sidebar')){
							dynamic_sidebar('discover-sidebar');
							}
						?>
					</div>
				</div>
				<div class="explore">
					<h2>Explore</h2>
					<p>We have digital resources for all your adventures</p>
					<div class="explore-menu">
						<?php
							if(is_active_sidebar('explore-sidebar')){
							dynamic_sidebar('explore-sidebar');
							}
						?>
					</div>
				</div>
			</div>
			<div class="new-arrivals-widget">
				<div class="hr"></div>
				<span><h3>New Arrivals</h3></span>
				<script type="text/javascript"
src="https://wowbrary.org/widgetslider.aspx?notitles&library=894&providertype=evergreen&width=80%25&widthbase=window&height=250&imagesize=M&imagescale=70&spacing=30&count=20&sidearrows&moreonly&borderstyle=none&headingstyle=display%3anone%3b&buttonstyle=modern&buttontextcolor=%23323232&target=_blank">
</script>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_footer();
