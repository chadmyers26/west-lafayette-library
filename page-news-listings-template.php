<?php
/**
 * Template Name: News Listings Pages Tpl
 *
 * This is the template that displays all the news blog posts.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package west-lafayette-library
 */

get_header();
?>

	<div id="news-wrapper" class="internal-content-area has-aside">
		<main id="main" class="site-main">
			<h1>News</h1>
				<?php $paged = (get_query_var('paged')) ? get_query_var('paged') : 1; ?>
				<?php $the_query = new WP_Query(array(
						'post_type'=>'post',
						'post_status'=>'publish',
						'posts_per_page'=>10,
						'paged' => $paged,
						'category__not_in' => array( 45 )
					));
				?>
				<?php if ( $the_query->have_posts() ) : ?>
				<ul class="news-items">
					<?php
					while ( $the_query->have_posts() ) : $the_query->the_post();
					?>
						<li>
							<div class="image-date-wrapper">
								<div class="image">
									<?php the_post_thumbnail(); ?>
								</div>
								<?php if( get_field('news-date') ): ?>
									<div class="date"><?php echo get_the_date('M d', get_the_ID()); ?></div>
								<?php endif; ?>
							</div>
							<div class="categories">
								<?php the_category(', '); ?>
							</div>
							<h2><?php the_title(); ?></h2>
							<a class="moretag" href="<?php echo get_post_permalink() ?>"> Read more</a>
						</li>
					<?php endwhile; ?>
				</ul>
				<nav class="pagination">
					<?php pagination_bar( $the_query ); ?>
					<div><a class="moretag" href="https://wlaf.lib.in.us/archived-articles/"> Archived Articles</a></div>
				</nav>
				<?php wp_reset_postdata(); ?>
				<?php else: ?>
				<?php endif; ?>
		</main><!-- #main -->
		<aside>
			<?php
				if(is_active_sidebar('news-sidebar')){
				dynamic_sidebar('news-sidebar');
				}
			?>
		</aside>
	</div><!-- #primary -->

<?php
get_footer();
