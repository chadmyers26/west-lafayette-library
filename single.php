<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package west-lafayette-library
 */

get_header();
?>
	<div id="news-wrapper" class="internal-content-area has-aside">
		<main id="main" class="site-main">
			<?php
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', get_post_type() );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
		<aside>
			<?php
				if(is_active_sidebar('news-sidebar')){
				dynamic_sidebar('news-sidebar');
				}
			?>
		</aside>
	</div><!-- #primary -->
<?php
get_footer();
