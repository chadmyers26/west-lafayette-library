<?php
/**
 * Template Name: About Pages Tpl
 *
 * This is the template that displays the about pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package west-lafayette-library
 */

get_header();
?>

	<div id="about-wrapper" class="internal-content-area has-aside">
		<main id="main" class="site-main">
			<?php
			$title = isset( $post->post_title ) ? $post->post_title : '';
			while ( have_posts() ) :
				the_post();

				get_template_part( 'template-parts/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
		<aside>
			<?php
				if(is_active_sidebar('about-sidebar')){
				dynamic_sidebar('about-sidebar');
				}
			?>
		</aside>
	</div><!-- #primary -->

<?php
get_footer();
