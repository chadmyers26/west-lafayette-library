/**
 * File notifications.js.
 *
 * Handles toggling the search bar and the main navigation, when the search icon is clicked.
 */
( function($) {
	"use strict";
	if (window.matchMedia("(max-width: 812px)").matches) {
		var notificationsCloseBar = document.getElementsByClassName("wpfront-close")[0];

		notificationsCloseBar.addEventListener("click", function() {
			setTimeout(function() {
				$("#wprmenu_bar").css({"position": "fixed"});
			}, 250);
		});

		setTimeout(function() {
			var notificationBar = $("#wpfront-notification-bar-spacer").css("height");

			if (parseInt(notificationBar) > 0) {
				$("#wprmenu_bar").css({"position": "absolute"});
			}
		}, 1200);
	}
} )(jQuery);
