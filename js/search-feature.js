/**
 * File search-feature.js.
 *
 * Handles toggling the search bar and the main navigation, when the search icon is clicked.
 */
( function($) {
	"use strict";

	var menuItemsContainer = document.getElementById("wprmenu_menu_ul");
	$(menuItemsContainer).append('<div id="search-feature" class="search"><button type="button" class="search-icon-button fas fa-search"></button><div id="search-inputs" class="search-inputs"><form class="mobile-search-form" role="search" method="get" target="_blank" id="mobile-search-form" action="http://evergreen.lib.in.us/eg/opac/results"><label class="mobile-search-field"><span class="screen-reader-text">What are you looking for?</span><input type="text" id="mobile-query" name="query" class="mobile-search-input form-control show" placeholder="What are you looking for?"/><input type="text" id="mobile-s" name="s" class="mobile-search-input form-control" placeholder="What are you looking for?" disabled="disabled"/></label><button type="submit" class="mobile-search-submit"></button></form><form class="mobile-radio-inputs"><div class="mobile-radio"><div class="mobile-option"><input type="radio" class="mobile-search-options" name="mobile-search-options" id="catalog" value=http://evergreen.lib.in.us/eg/opac/results" data-input="query" checked=""><label for="catalog">Catalog</label></div><div class="mobile-option"><input type="radio" class="mobile-search-options" name="mobile-search-options" id="website" value="http://westlafayettelibrary.flywheelsites.com" data-input="s"><label for="website">Website</label></div></div></form></div></div></div>');

	var container, button, search, mainMenu, radioButtonOptions, mobileMenuContainer, mobileSearchButton, mobileSearch, menuItemsContainer, mobileRadioButtonOptions;

	mobileMenuContainer = document.getElementById("wprmenu_menu_ul");
	mobileSearchButton = mobileMenuContainer.getElementsByClassName("search-icon-button")[0];
	mobileSearch = mobileMenuContainer.getElementsByClassName("search-inputs")[0];
	mobileRadioButtonOptions = document.getElementsByName("mobile-search-options");

	container = document.getElementById("search-feature");
	if ( ! container ) {
		return;
	}

	button = container.getElementsByClassName("search-icon-button")[0];
	if ("undefined" === typeof button ) {
		return;
	}

	search = document.getElementById("search-inputs");
	if ( ! search ) {
		return;
	}

	mainMenu = document.getElementById("site-navigation");
	if ( ! mainMenu ) {
		return;
	}

	radioButtonOptions = document.getElementsByName("search-options");

	function hasClass(element, className) {
		return (' ' + element.className + ' ').indexOf(' ' + className+ ' ') > -1;
	}

	button.onclick = function(event, target) {
		var clickedTarget = event.target;

		// If button has the class of 'fa-search' then toggle the button class to 'fa-times'
		// toggle the navigation not show - remove the class of 'show'
		// toggle the search bar to 'show'
		if (hasClass(clickedTarget, "fa-search")) {
			clickedTarget.className = clickedTarget.className.replace("search-icon-button fas fa-search", "search-icon-button fas fa-times");
			mainMenu.className = mainMenu.className.replace("main-navigation show", "main-navigation");
			search.className = "search-inputs show";
		} else {
			clickedTarget.className = clickedTarget.className.replace("search-icon-button fas fa-times", "search-icon-button fas fa-search");
			search.className = search.className.replace("search-inputs show", "search-inputs");
			mainMenu.className = "main-navigation show";
			document.getElementById("query").value = "";
			document.getElementById("s").value = "";
		}
	};

	mobileSearchButton.onclick = function(event, target) {
		var clickedTarget = event.target;

		// If button has the class of 'fa-search' then toggle the button class to 'fa-times'
		// toggle the navigation not show - remove the class of 'show'
		// toggle the search bar to 'show'
		if (hasClass(clickedTarget, "fa-search")) {
			clickedTarget.className = clickedTarget.className.replace("search-icon-button fas fa-search", "search-icon-button fas fa-times");
			mobileSearch.className = "search-inputs show";
		} else {
			clickedTarget.className = clickedTarget.className.replace("search-icon-button fas fa-times", "search-icon-button fas fa-search");
			mobileSearch.className = search.className.replace("search-inputs show", "search-inputs");
			document.getElementById("query").value = "";
			document.getElementById("s").value = "";
		}
	};

	function mobileRadioClickEvent(event, target) {
		// Based on which radio button is checked, change the action on the form to search the catalog or the website accordingly.
		var searchForm = document.getElementById("mobile-search-form");
		var catalogInput = document.getElementById("mobile-query");
		var searchInput = document.getElementById("mobile-s");

		if (event.currentTarget.value === "http://evergreen.lib.in.us/eg/opac/results" ) {
			searchForm.action = event.currentTarget.value;
			searchForm.target = "_blank";
			catalogInput.removeAttribute("disabled");
			catalogInput.className = catalogInput.className.replace("mobile-search-input form-control", "mobile-search-input form-control show");
			searchInput.setAttribute("disabled", "disabled");
			searchInput.className = searchInput.className.replace("mobile-search-input form-control show", "mobile-search-input form-control");
		} else {
			searchForm.action = event.currentTarget.value;
			searchForm.target = "";
			document.getElementById("mobile-query").setAttribute("disabled", "disabled");
			document.getElementById("mobile-s").removeAttribute("disabled");
			catalogInput.setAttribute("disabled", "disabled");
			catalogInput.className = catalogInput.className.replace("mobile-search-input form-control show", "mobile-search-input form-control");
			searchInput.removeAttribute("disabled");
			searchInput.className = searchInput.className.replace("mobile-search-input form-control", "mobile-search-input form-control show");
		}
	};

	function radioClickEvent(event, target) {
		// Based on which radio button is checked, change the action on the form to search the catalog or the website accordingly.
		var searchForm = document.getElementById("search-form");
		var catalogInput = document.getElementById("query");
		var searchInput = document.getElementById("s");

		if (event.currentTarget.value === "http://evergreen.lib.in.us/eg/opac/results" ) {
			searchForm.action = event.currentTarget.value;
			searchForm.target = "_blank";
			catalogInput.removeAttribute("disabled");
			catalogInput.className = catalogInput.className.replace("search-input form-control", "search-input form-control show");
			searchInput.setAttribute("disabled", "disabled");
			searchInput.className = searchInput.className.replace("search-input form-control show", "search-input form-control");
		} else {
			searchForm.action = event.currentTarget.value;
			searchForm.target = "";
			document.getElementById("query").setAttribute("disabled", "disabled");
			document.getElementById("s").removeAttribute("disabled");
			catalogInput.setAttribute("disabled", "disabled");
			catalogInput.className = catalogInput.className.replace("search-input form-control show", "search-input form-control");
			searchInput.removeAttribute("disabled");
			searchInput.className = searchInput.className.replace("search-input form-control", "search-input form-control show");
		}
	};

	mobileRadioButtonOptions[0].onclick = mobileRadioClickEvent;
	mobileRadioButtonOptions[1].onclick = mobileRadioClickEvent;
	radioButtonOptions[0].onclick = radioClickEvent;
	radioButtonOptions[1].onclick = radioClickEvent;
} )(jQuery);
