<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package west-lafayette-library
 */

get_header();
?>

	<div id="not-found" class="internal-content-area">
		<main id="main" class="site-main">

			<section class="error-404 not-found">
				<header class="page-header">
					<h1 class="page-title">Oops!</h1>
				</header><!-- .page-header -->

				<div class="page-content">
					<p class="page-not-found">404 - Page not found</p>
					<p>The page you are looking for might have been removed, had its name changed or is temporarily unavailable.</p>
					<a href="/" class="home-button">Home</a>
				</div><!-- .page-content -->
			</section><!-- .error-404 -->

		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
