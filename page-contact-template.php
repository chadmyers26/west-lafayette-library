<?php
/**
 * Template Name: Contact Pages Tpl
 *
 * This is the template that displays the Contact pages.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package west-lafayette-library
 */

get_header();
?>

	<div id="contact-wrapper" class="internal-content-area">
		<?php
			if(is_active_sidebar('contact-image-bar')){
			dynamic_sidebar('contact-image-bar');
			}
		?>
		<main id="main" class="site-main">
			<?php
			$title = isset( $post->post_title ) ? $post->post_title : '';

			while ( have_posts() ) :
				the_post();
			?>

			<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

				<?php west_lafayette_library_post_thumbnail(); ?>

				<div class="entry-content">
					<div class="contact-operations-info">
						<?php
							if(is_active_sidebar('contact-operations-info')){
							dynamic_sidebar('contact-operations-info');
							}
						?>
					</div>

					<div class="contact-content">
					<?php
						the_content();

						wp_link_pages( array(
							'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'west-lafayette-library' ),
							'after'  => '</div>',
						) );
						?>
					</div>
				</div><!-- .entry-content -->

				<?php if ( get_edit_post_link() ) : ?>
					<footer class="entry-footer">
						<?php
						edit_post_link(
							sprintf(
								wp_kses(
									/* translators: %s: Name of current post. Only visible to screen readers */
									__( 'Edit <span class="screen-reader-text">%s</span>', 'west-lafayette-library' ),
									array(
										'span' => array(
											'class' => array(),
										),
									)
								),
								get_the_title()
							),
							'<span class="edit-link">',
							'</span>'
						);
						?>
					</footer><!-- .entry-footer -->
				<?php endif; ?>
			</article><!-- #post-<?php the_ID(); ?> -->

			<?php	if ( comments_open() || get_comments_number() ) :
					comments_template();
				endif;

			endwhile; // End of the loop.
			?>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
