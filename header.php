<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package west-lafayette-library
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
	<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'west-lafayette-library' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
		<div class="site-branding">
			<?php
			the_custom_logo();
			if ( is_front_page() && is_home() ) :
				?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
				<?php
			else :
				?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
				<?php
			endif;
			$west_lafayette_library_description = get_bloginfo( 'description', 'display' );
			if ( $west_lafayette_library_description || is_customize_preview() ) :
				?>
				<p class="site-description"><?php echo $west_lafayette_library_description; /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
		</div><!-- .site-branding -->

		<div class="navigation">
			<nav id="site-secondary-navigation" class="secondary-navigation" role="navigation" aria-label="<?php _e( "primary-menu", "textdomain" ); ?>">
				<?php
					wp_nav_menu( array(
						'theme_location' => 'menu-2',
						'menu_id'        => 'secondary-menu',
					) );
				?>
			</nav><!-- #site-navigation -->

			<div id="main-search" class="main-nav-search">
				<nav id="site-navigation" class="main-navigation show" role="navigation" aria-label="<?php _e( "primary-menu", "textdomain" ); ?>">
					<?php
						wp_nav_menu( array(
							'theme_location' => 'menu-1',
							'container' => false,
							'menu_id'        => 'primary-menu',
							'walker'         => new Aria_Walker_Nav_Menu(),
							'items_wrap' => '<ul id="%1$s" class="%2$s" role="menubar">%3$s</ul>'
						) );
					?>
				</nav><!-- #site-navigation -->

				<div id="search-feature" class="search">
					<button type="button" class="search-icon-button fas fa-search"></button>
					<?php get_search_form(); ?>
				</div>
			</div>
		</div>
	</header><!-- #masthead -->

	<div id="content" class="site-content">
