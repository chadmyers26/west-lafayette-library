<?php require_once "walker-aria/aria-walker-nav-menu.php"; ?>

<?php
/**
 * west-lafayette-library functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package west-lafayette-library
 */

if ( ! function_exists( 'west_lafayette_library_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function west_lafayette_library_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on west-lafayette-library, use a find and replace
		 * to change 'west-lafayette-library' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'west-lafayette-library', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'west-lafayette-library' ),
			'menu-2' => esc_html__( 'Secondary', 'west-lafayette-library' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'west_lafayette_library_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'west_lafayette_library_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function west_lafayette_library_content_width() {
	// This variable is intended to be overruled from themes.
	// Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
	// phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
	$GLOBALS['content_width'] = apply_filters( 'west_lafayette_library_content_width', 640 );
}
add_action( 'after_setup_theme', 'west_lafayette_library_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function west_lafayette_library_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'west-lafayette-library' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'west-lafayette-library' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'west_lafayette_library_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function west_lafayette_library_scripts() {
	wp_enqueue_style( 'west-lafayette-library-style', get_stylesheet_uri() );
	wp_enqueue_style( 'twentyseventeen-colors-dark', get_theme_file_uri( '/assets/css/west-lafayette-custom-styles.css' ));

	wp_enqueue_script( 'west-lafayette-library-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20151215', true );
	wp_enqueue_script( 'west-lafayette-library-search-feature', get_template_directory_uri() . '/js/search-feature.js', array(), '20151215', true );
	wp_enqueue_script( 'west-lafayette-library-notifications', get_template_directory_uri() . '/js/notifications.js', array(), '20151215', true );
	wp_enqueue_script( 'textdomain-wai-aria', get_template_directory_uri() . '/walker-aria/wai-aria.js', array( 'jquery' ), null );

	wp_enqueue_script( 'west-lafayette-library-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'west_lafayette_library_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

// add arrows to menu parent
function oenology_add_menu_parent_class( $items ) {

	$parents = array();
	foreach ( $items as $item ) {
		if ( $item->menu_item_parent && $item->menu_item_parent > 0 ) {
			$parents[] = $item->menu_item_parent;
		}
	}

	foreach ( $items as $item ) {
		if ( in_array( $item->ID, $parents ) ) {
			$item->classes[] = 'has-children';
		}
	}

	return $items;
}
add_filter( 'wp_nav_menu_objects', 'oenology_add_menu_parent_class' );

function html5_search_form( $form ) {
	$form = '<div id="search-inputs" class="search-inputs">
	<form class="search-form" role="search" method="get" target="_blank" id="search-form" action="http://evergreen.lib.in.us/eg/opac/results">
		<label class="search-field">
			<span class="screen-reader-text">What are you looking for?</span>
			<input type="text" id="query" name="query" class="search-input form-control show" placeholder="What are you looking for?"/>
			<input type="text" id="s" name="s" class="search-input form-control" placeholder="What are you looking for?" disabled="disabled"/>
		</label>
		<button type="submit" class="search-submit"></button>
	</form>
	<form class="radio-inputs">
		<div class="radio">
			<div class="option">
				<input type="radio" class="search-options" name="search-options" id="catalog" value=http://evergreen.lib.in.us/eg/opac/results" data-input="query" checked="">
				<label for="catalog">
					Catalog
				</label>
			</div>
			<div class="option">
				<input type="radio" class="search-options" name="search-options" id="website" value="http://westlafayettelibrary.flywheelsites.com" data-input="s">
				<label for="website">
					Website
				</label>
			</div>
		</div>
	</form>
</div>';
	return $form;
}

add_filter( 'get_search_form', 'html5_search_form' );

register_sidebar( array(
	'name' => 'Footer Left',
	'id' => 'footer-left',
	'description' => 'Appears in the footer area on the left hand side',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'name' => 'Footer Center',
	'id' => 'footer-center',
	'description' => 'Appears in the footer area in the center',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'name' => 'Footer Right',
	'id' => 'footer-right',
	'description' => 'Appears in the footer area on the right side',
	'before_widget' => '<aside id="%1$s" class="widget %2$s">',
	'after_widget' => '</aside>',
	'before_title' => '<h3 class="widget-title">',
	'after_title' => '</h3>',
) );

register_sidebar( array(
	'name' => 'About Sidebar',
	'id' => 'about-sidebar',
	'description' => 'Appears on the about pages in the sidebar',
	'before_title' => '<h2 class="internal-menu-titles">',
	'after_title' => '</h2>',
) );

register_sidebar( array(
	'name' => 'Browse Sidebar',
	'id' => 'browse-sidebar',
	'description' => 'Appears on the browse pages in the sidebar',
	'before_title' => '<h2 class="internal-menu-titles">',
	'after_title' => '</h2>',
) );

register_sidebar( array(
	'name' => 'News Sidebar',
	'id' => 'news-sidebar',
	'description' => 'Appears on the news pages in the sidebar',
	'before_title' => '<h2 class="internal-menu-titles">',
	'after_title' => '</h2>',
) );

register_sidebar( array(
	'name' => 'Calendar Sidebar',
	'id' => 'calendar-sidebar',
	'description' => 'Appears on the calendar pages in the sidebar',
	'before_title' => '<h2 class="internal-menu-titles">',
	'after_title' => '</h2>',
) );

register_sidebar( array(
	'name' => 'Services Sidebar',
	'id' => 'services-sidebar',
	'description' => 'Appears on the services pages in the sidebar',
	'before_title' => '<h2 class="internal-menu-titles">',
	'after_title' => '</h2>',
) );

register_sidebar( array(
	'name' => 'Contact Page Image',
	'id' => 'contact-image-bar',
	'description' => 'Appears on the contact page',
	'before_title' => '<div class="contact-page-image">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Contact Page Operations Info',
	'id' => 'contact-operations-info',
	'description' => 'Appears on the contact page',
	'before_title' => '<div class="contact-operations-info">',
	'after_title' => '</div>',
) );

register_sidebar( array(
	'name' => 'Gather Sidebar',
	'id' => 'gather-sidebar',
	'description' => 'Appears on the home page in the Gather box',
	'before_widget' => '<aside id="%1$s" class="gather-menu widget %2$s">',
	'after_widget' => '</aside>',
) );

register_sidebar( array(
	'name' => 'Discover Sidebar',
	'id' => 'discover-sidebar',
	'description' => 'Appears on the home page in the Discover box',
	'before_widget' => '<aside id="%1$s" class="discover-menu widget %2$s">',
	'after_widget' => '</aside>',
) );

register_sidebar( array(
	'name' => 'Explore Sidebar',
	'id' => 'explore-sidebar',
	'description' => 'Appears on the home page in the Explore box',
	'before_widget' => '<aside id="%1$s" class="explore-menu widget %2$s">',
	'after_widget' => '</aside>',
) );

function pagination_bar( $custom_query ) {
	$total_pages = $custom_query->max_num_pages;
	$big = 999999999; // need an unlikely integer

	if ($total_pages > 1){
		$current_page = max(1, get_query_var('paged'));

		echo paginate_links(array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => $current_page,
			'total' => $total_pages,
		));
	}
}

function update_post( ) {
	global $wpdb;
	
	$query = "INSERT INTO wp_4w24ffkyg5_term_relationships (object_id,term_taxonomy_id,term_order)
	SELECT wp_4w24ffkyg5_posts.id, 45, 0
	FROM wp_4w24ffkyg5_posts
	INNER JOIN wp_4w24ffkyg5_term_relationships ON wp_4w24ffkyg5_term_relationships.object_id=wp_4w24ffkyg5_posts.ID
	INNER JOIN wp_4w24ffkyg5_term_taxonomy ON wp_4w24ffkyg5_term_taxonomy.term_taxonomy_id = wp_4w24ffkyg5_term_relationships.term_taxonomy_id
	INNER JOIN wp_4w24ffkyg5_terms on wp_4w24ffkyg5_terms.term_id = wp_4w24ffkyg5_term_taxonomy.term_id
	INNER JOIN wp_4w24ffkyg5_postmeta on wp_4w24ffkyg5_postmeta.post_id = wp_4w24ffkyg5_posts.id
	WHERE wp_4w24ffkyg5_term_taxonomy.taxonomy='category' and wp_4w24ffkyg5_postmeta.meta_key = 'expiration_date' and name <> 'archived' and DATE(wp_4w24ffkyg5_postmeta.meta_value) < DATE(NOW())";
	
	$results = $wpdb->get_results($query);
 }
 add_action( 'update_posts_status', 'update_post' );